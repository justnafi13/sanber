<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class PertanyaanController extends Controller
{
    public function index()
    {
        // foreignkey profile_id & jawaban_id saya NULL dulu.
        $pertanyaan = DB::table('pertanyaan')->orderBy('created_at')->get();
        // dd($pertanyaan);
        return view('pertanyaan', ['pertanyaan' => $pertanyaan]);
    }

    public function create()
    {
        return view('pertanyaaninput');
    }

    public function store(Request $request)
    {
        DB::table('pertanyaan')->insert([
            'judul'         => $request->judul,
            'isi'           => $request->isi,
            'created_at'    => \Carbon\Carbon::now(),
            'updated_at'    => \Carbon\Carbon::now(),
        ]);
        return redirect('pertanyaan');
    }

    public function show($id)
    {
        $pertanyaan = DB::table('pertanyaan')->where('id',$id)->get();
        // dd($pertanyaan);
        return view('pertanyaandetail', ['pertanyaan' => $pertanyaan]);
    }

    public function edit($id)
    {
        $pertanyaan = DB::table('pertanyaan')->where('id',$id)->get();
        // dd($pertanyaan);
        return view('pertanyaanedit', ['pertanyaan' => $pertanyaan]);
    }

    public function update(Request $request)
    {
        // dd($request->id);
        DB::table('pertanyaan')->where('id',$request->id)->update
        ([
            'judul'         => $request->judul,
            'isi'           => $request->isi,
            'updated_at'    => \Carbon\Carbon::now(),
        ]);
        return redirect('pertanyaan');
    }

    public function destroy($id)
    {
        // dd('stop');
        $pertanyaan = DB::table('pertanyaan')->where('id',$id)->delete();
        // dd($pertanyaan); 
        return redirect('pertanyaan');
    }
}
