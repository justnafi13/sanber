@extends('layouts.app')

@section('title', 'Pertanyaan')

@section('content')
<div class="card">
    <div class="card-header">
    <h3 class="card-title">Data Pertanyaan</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
    <div class="">
        <a href="{{ url('pertanyaan/create') }}" class="btn btn-primary mb-3" role="button">Tambah Data</a>
    </div>
    <table id="example1" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>Id</th>
                <th>Judul</th>
                <th style="width:400px;">Isi</th>
                <th>Created</th>
                <th>Updated</th>
                <th colspan="2" class="text-center">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($pertanyaan as $pertanyaan)
            <tr>
                <td>{{ $pertanyaan->id }}</td>
                <td><a href="/pertanyaan/{{ $pertanyaan->id }}" target="_blank" rel="noopener noreferrer">{{ $pertanyaan->judul }}</a></td>
                <td>{{ $pertanyaan->isi }}</td>
                <td>{{ $pertanyaan->created_at }}</td>
                <td>{{ $pertanyaan->updated_at }}</td>
                <td>
                    <a class="btn" href="/pertanyaan/{{ $pertanyaan->id }}/edit"><i class="fas fa-edit"></i></a>
                </td>
                <td>
                <form action="/pertanyaan/{{ $pertanyaan->id }}" method="POST">
                @csrf
                    <input type="hidden" name="_method" value="DELETE">
                    <button class="btn"><i class="fas fa-eraser"></i></button>
                </form>
                </td>
            </tr>
            @endforeach
        </tbody>
        </table>
        </div>
        <!-- /.card-body -->
</div>
@endsection

@section('script')
<script src="{{ asset('datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
@endsection