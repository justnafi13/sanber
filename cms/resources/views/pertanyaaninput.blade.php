@extends('layouts.app')

@section('title', 'Input Pertanyaan')

@section('content')
<form method="POST" action="{{ url('pertanyaan') }}">
    @csrf
    <div class="form-group">
        <label for="title">Title</label>
        <input class="form-control" id="judul" placeholder="Sanbercode" name="judul">
    </div>
    <div class="form-group">
        <label for="desc">Isi</label>
        <textarea class="form-control" id="desc" rows="4" name="isi"></textarea>
    </div>

    <div class="form-group">
        <input type="submit" class="btn btn-primary" value="Simpan Data">
    </div>
</form>
@endsection

@section('script')
<script src="{{ asset('datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
@endsection
