@extends('layouts.app')

@section('title', 'Pertanyaan Detail')

@section('content')
<div class="card text-center">
@foreach($pertanyaan as $pertanyaan)
  <div class="card-header">
   <strong>{{ $pertanyaan->judul }}</strong>
  </div>
  <div class="card-body">
    <p class="card-text">{{ $pertanyaan->isi }}</p>
    <p class="card-text">{{ $pertanyaan->created_at }}</p>
  </div>
@endforeach
</div>
@endsection

@section('script')
@endsection