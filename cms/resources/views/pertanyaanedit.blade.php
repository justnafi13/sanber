@extends('layouts.app')

@section('title', 'Input Pertanyaan')

@section('content')
@foreach($pertanyaan as $pertanyaan)
<form method="POST" action="{{ url('pertanyaan/'.$pertanyaan->id) }}">
    @csrf
    <input type="hidden" name="_method" value="PUT">
    <div class="form-group">
        <label for="title">Title</label>
        <input class="form-control" id="judul" placeholder="Sanbercode" name="judul" value="{{ $pertanyaan->judul }}">
    </div>
    <div class="form-group">
        <label for="desc">Isi</label>
        <textarea class="form-control" id="desc" rows="4" name="isi">{{ $pertanyaan->isi }}</textarea>
    </div>

    <div class="form-group">
        <input type="submit" class="btn btn-primary" value="Simpan Data">
    </div>
</form>
@endforeach
@endsection

@section('script')
<script src="{{ asset('datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
@endsection
